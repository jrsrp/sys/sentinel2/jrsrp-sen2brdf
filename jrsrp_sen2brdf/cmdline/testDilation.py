#!/usr/bin/env python
# Licensing. MIT License
#
# Copyright (c) 2020 JRSRP
# See LICENSE file
#
"""
Testing angle image dilation
"""
from __future__ import print_function, division

import sys
import argparse
import zipfile

import numpy
try:
    from osgeo import gdal
except ImportError:
    print("GDAL Python binding not available. Tests cannot be performed. ")
    sys.exit()

from jrsrp_sen2brdf import sen2l2a_brdf

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("zip1", help="L2A zipfile")
    p.add_argument("zip2", help="L2A zipfile")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    cmdargs = getCmdargs()
    ai1 = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip1)
    ai2 = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip2)
    ai1_nd = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip1, dilateAngles=False)
    ai2_nd = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip2, dilateAngles=False)
    
    proj = getProjection(cmdargs.zip1)
    writeViewZenB02('viewZen_1_b02.tif', ai1, proj)
    writeViewZenB02('viewZen_2_b02.tif', ai2, proj)
    writeViewZenB02('viewZen_1_b02_nd.tif', ai1_nd, proj)
    writeViewZenB02('viewZen_2_b02_nd.tif', ai2_nd, proj)


def writeViewZenB02(imgfile, ai, proj):
    """
    From the given angle info object, write the B02 view zenith
    to the given imgfile
    """
    viewZen = ai.viewZenithDict['B02']
    (nrows, ncols) = viewZen.shape
    (ulx, uly) = ai.anglesULXY
    
    drvr = gdal.GetDriverByName('GTiff')
    ds = drvr.Create(imgfile, ncols, nrows, 1, gdal.GDT_Float32, ['COMPRESS=DEFLATE'])
    ds.SetProjection(proj)
    geotransform = (ulx, ai.angleGridXres, 0.0, uly, 0.0, -ai.angleGridYres)
    ds.SetGeoTransform(geotransform)
    band = ds.GetRasterBand(1)
    band.WriteArray(viewZen.astype(numpy.float32))


def getProjection(zipfilename):
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    fileEnding = "_{}_{}m.jp2".format('B02', 10)
    imgfile = [fn for fn in filenames if fn.endswith(fileEnding)][0]

    # Use GDAL to read the image
    ds = gdal.Open("/vsizip/{}/{}".format(zipfilename, imgfile))
    return ds.GetProjection()
                                
                                
if __name__ == "__main__":
    main()
