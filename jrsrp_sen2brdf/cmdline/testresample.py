#!/usr/bin/env python
# Licensing. MIT License
#
# Copyright (c) 2020 JRSRP
# See LICENSE file
#
"""
Simple test of the resampling code. Give it a simple L2A zip file,
and it will write out the angle grid as an image, and the resampled version
of the same grid. They should overlay exactly in a GIS viewer. 

"""
from __future__ import print_function, division

import sys
import os
import argparse
import zipfile

import numpy
try:
    from osgeo import gdal
except ImportError:
    print("GDAL Python binding not available. Tests cannot be performed. ")
    sys.exit()

from jrsrp_sen2brdf import sen2l2a_brdf

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("zipfile", help="L2A zipfile")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    angleInfo = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zipfile)
    
    print('Calculating resample indexes .... ', end='')
    resample10m = angleInfo.calcResampleNdx(10)
    print('Done\n')
    
    proj = getProjection(cmdargs.zipfile)
    bandName = 'B02'
    pixSize = 10
    
    viewZenith = angleInfo.viewZenithDict[bandName]
    viewZenith10m = viewZenith[resample10m]
    
    writeImg('viewzen_b02.tif', angleInfo.anglesULXY, proj, viewZenith, angleInfo.angleGridXres)
    writeImg('viewzen_b02_10m.tif', angleInfo.ulxyByRes['10'], proj, viewZenith10m, pixSize)


def writeImg(imgfile, ulxy, proj, img, pixSize):
    """
    Write the given array to the given image file
    """
    print("Writing", imgfile)
    (nrows, ncols) = img.shape
    (ulx, uly) = ulxy
    
    drvr = gdal.GetDriverByName('GTiff')
    ds = drvr.Create(imgfile, ncols, nrows, 1, gdal.GDT_Float32, ['COMPRESS=DEFLATE'])
    ds.SetProjection(proj)
    geoTransform = (ulx, pixSize, 0, uly, 0, -pixSize)
    ds.SetGeoTransform(geoTransform)
    band = ds.GetRasterBand(1)
    band.WriteArray(img.astype(numpy.float32))

def getProjection(zipfilename):
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    fileEnding = "_{}_{}m.jp2".format('B02', 10)
    imgfile = [fn for fn in filenames if fn.endswith(fileEnding)][0]
    
    # Use GDAL to read the image
    ds = gdal.Open("/vsizip/{}/{}".format(zipfilename, imgfile))
    return ds.GetProjection()


if __name__ == "__main__":
    main()
