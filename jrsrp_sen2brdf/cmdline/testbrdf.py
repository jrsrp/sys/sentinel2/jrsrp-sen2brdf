#!/usr/bin/env python
# Licensing. MIT License
#
# Copyright (c) 2020 JRSRP
# See LICENSE file
#
"""
Simple test of the BRDF correction code, on a single pair of overlapping
images. Tested on the pair
    S2A_MSIL2A_20200611T001121_N0214_R073_T55JFL_20200611T022818.zip
    S2B_MSIL2A_20200609T002059_N0214_R116_T55JFL_20200609T021522.zip
The pair used should be of 'typical' vegetation cover, i.e. not over large
salt pans or similar. 

"""
from __future__ import print_function, division

import sys
import os
import argparse
import zipfile
import time

import numpy
try:
    from osgeo import gdal
except ImportError:
    print("GDAL Python binding not available. Tests cannot be performed. ")
    sys.exit()

from jrsrp_sen2brdf import sen2l2a_brdf

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("zip1", help="L2A zipfile")
    p.add_argument("zip2", help="L2A zipfile")
    p.add_argument("--useroy2017", default=False, action="store_true")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    angleInfo1 = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip1)
    angleInfo2 = sen2l2a_brdf.AngleInfo(zipfilename=cmdargs.zip2)
    
    bandList10m = ['B02', 'B03', 'B04', 'B08']
    bandList20m = ['B05', 'B06', 'B07', 'B8A', 'B11', 'B12']
    if cmdargs.useroy2017:
        bandList20m = ['B11', 'B12']
    
    s = time.time()
    print('Calculating resample indexes .... ', end='')
    resample10m_1 = angleInfo1.calcResampleNdx(10)
    resample10m_2 = angleInfo2.calcResampleNdx(10)
    resample20m_1 = angleInfo1.calcResampleNdx(20)
    resample20m_2 = angleInfo2.calcResampleNdx(20)
    elapsedTime = time.time() - s
    print('Done in {:.1f} seconds\n'.format(elapsedTime))

    print("MAD = Mean Absolute Difference. Abs diff per pixel, averaged")
    print("diffMeanRef = difference between mean reflectance over all pixels")
    print("(adj) = after BRDF adjustment")
    print()
    print("Band     MAD      MAD(adj)   diffMeanRef  diffMeanRef(adj)   Flags")

    for bandName in bandList10m:
        (gamma, gammaImg) = compareBand(10, cmdargs.zip1, cmdargs.zip2, bandName, angleInfo1, 
            angleInfo2, resample10m_1, resample10m_2, cmdargs.useroy2017)

    for bandName in bandList20m:
        compareBand(20, cmdargs.zip1, cmdargs.zip2, bandName, angleInfo1, 
            angleInfo2, resample20m_1, resample20m_2, cmdargs.useroy2017)
    
    print("\nFlags")
    print("    ^ = Impovement in both measures")
    print("    ? = Improvement in only one measure")
    print("    X = No improvement in either measure")
    print("    # = Adjustment caused a change in pixel count (i.e. very bad)")


def compareBand(pixSize, zip1, zip2, bandName, angleInfo1, angleInfo2, 
        resample1, resample2, useroy2017):
    """
    Compare adjusted and non-adjusted values for the given band, 
    between the two given files. 
    """
    sunZenStd = 45.0
    whichCoeffs = sen2l2a_brdf.BRDFCOEFFS_FLOOD2020
    if useroy2017:
        whichCoeffs = sen2l2a_brdf.BRDFCOEFFS_ROY2017

    gamma1 = angleInfo1.calcStdAdjustFactor(sunZenStd, bandName, whichCoeffs=whichCoeffs)
    gamma2 = angleInfo2.calcStdAdjustFactor(sunZenStd, bandName, whichCoeffs=whichCoeffs)
    
    gammaImg1 = gamma1[resample1]
    gammaImg2 = gamma2[resample2]
    
    img1 = readImg(zip1, bandName, pixSize)
    img2 = readImg(zip2, bandName, pixSize)
    
    img1_adj = img1 * gammaImg1
    img2_adj = img2 * gammaImg2
    
    reportDiff(bandName, img1, img1_adj, img2, img2_adj)
    
    return (gamma1, gammaImg1)


def readImg(zipfilename, bandName, pixSize):
    """
    Read a single band from the given zip file
    Return a numpy array of reflectance, scaled to reflectance units [0-1]
    """
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    fileEnding = "_{}_{}m.jp2".format(bandName, pixSize)
    imgfile = [fn for fn in filenames if fn.endswith(fileEnding)][0]
    
    # Use GDAL to read the image
    ds = gdal.Open("/vsizip/{}/{}".format(zipfilename, imgfile))
    b = ds.GetRasterBand(1)
    img = b.ReadAsArray()
    
    return (img * 0.0001)


def getProjection(zipfilename):
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    fileEnding = "_{}_{}m.jp2".format('B02', 10)
    imgfile = [fn for fn in filenames if fn.endswith(fileEnding)][0]
    
    # Use GDAL to read the image
    ds = gdal.Open("/vsizip/{}/{}".format(zipfilename, imgfile))
    return ds.GetProjection()


def reportDiff(bandName, img1, img1_adj, img2, img2_adj):
    """
    Report on the difference between img1 and img2, for adjusted and 
    non-adjusted cases.
    """
    (diff, pixCount, mask) = imgDiff(img1, img2)
    (diffAdj, pixCountAdj, maskAdj) = imgDiff(img1_adj, img2_adj)
    
    fullMask = (mask & maskAdj)
    meanRef1 = img1[fullMask].mean()
    meanRef1adj = img1_adj[fullMask].mean()
    meanRef2 = img2[fullMask].mean()
    meanRef2adj = img2_adj[fullMask].mean()
    
    # Record what sort of improvement we get (if any)
    diffImprov = (diffAdj < diff)
    meanImprov = (abs(meanRef1adj - meanRef2adj) < abs(meanRef1 - meanRef2))
    clearImprov = diffImprov and meanImprov
    ambiguousImprov = (not clearImprov) and (diffImprov or meanImprov)
    if diffImprov and meanImprov:
        improvFlag = "^"
    elif diffImprov or meanImprov:
        improvFlag = "?"
    else:
        improvFlag = "X"
    
    pixOKflag = " "
    if (pixCount != pixCountAdj):
        pixOKflag = "#"
    
    print("{}    {:7.4f}    {:7.4f}     {:7.4f}         {:7.4f}         {}{}".format(bandName, 
        diff, diffAdj, meanRef1-meanRef2, meanRef1adj-meanRef2adj, improvFlag, pixOKflag))


def imgDiff(img1, img2):
    """
    Calculate difference between two images. Relies on the datatype
    being a signed integer. 
    """
    absDiff = numpy.absolute(img1 - img2)
    mask = ((img1 != 0) & (img2 != 0) & 
        (~numpy.isnan(img1)) & (~numpy.isnan(img2)))
    pixCount = numpy.count_nonzero(mask)
    meanAbsDiff = numpy.mean(absDiff[mask])
    return (meanAbsDiff, pixCount, mask)


if __name__ == "__main__":
    main()
