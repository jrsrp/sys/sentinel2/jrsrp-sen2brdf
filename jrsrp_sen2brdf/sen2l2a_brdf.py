# Licensing. MIT License
#
# Copyright (c) 2020 JRSRP
# See LICENSE file
#
"""
Apply a BRDF normalization to ESA's Sentinel-2 Level-2A
surface reflectance imagery. 

See Flood (2020) for discussion of why this is useful, and 
how the coefficients were fitted. 
https://figshare.com/articles/online_resource/Assessing_BRDF_effects_in_the_Sentinel-2_Level_2A_product_in_the_Australian_landscape/12820268

Also includes a class to handle the angle information from 
the XML GRANULE metadata file. Load the sun and satellite 
angle grids from the XML file. Resampling (nearest neighbour) 
from 5km grids to requested pixel grid. 

"""
from __future__ import print_function, division

import os
import zipfile
from collections import namedtuple
from xml.dom import minidom

import numpy

BRDFCOEFFS_FLOOD2020 = "Flood2020"
BRDFCOEFFS_ROY2017 = "Roy2017"
BRDFCOEFFS_DEFAULT = BRDFCOEFFS_FLOOD2020

BrdfCoeffs = namedtuple('BrdfCoeffs', ['fGeo', 'fVol', 'fIso'])
# First select by coefficient group, then by band name
brdfCoeffsDict = {
    # From Flood (2020), Table 1
    BRDFCOEFFS_FLOOD2020:{
        'B02':BrdfCoeffs(fIso=1.0, fGeo=0.3087, fVol=0.3399),
        'B03':BrdfCoeffs(fIso=1.0, fGeo=0.1970, fVol=0.6527),
        'B04':BrdfCoeffs(fIso=1.0, fGeo=0.1564, fVol=0.4404),
        'B05':BrdfCoeffs(fIso=1.0, fGeo=0.1455, fVol=0.5411),
        'B06':BrdfCoeffs(fIso=1.0, fGeo=0.1083, fVol=0.6793),
        'B07':BrdfCoeffs(fIso=1.0, fGeo=0.1078, fVol=0.6705),
        'B08':BrdfCoeffs(fIso=1.0, fGeo=0.0868, fVol=0.8015),
        'B8A':BrdfCoeffs(fIso=1.0, fGeo=0.1094, fVol=0.6251),
        'B11':BrdfCoeffs(fIso=1.0, fGeo=0.1500, fVol=0.3216),
        'B12':BrdfCoeffs(fIso=1.0, fGeo=0.1753, fVol=0.2466)},
    # From Roy et al, 2017, table 1. 
    BRDFCOEFFS_ROY2017:{
        'B02':BrdfCoeffs(fIso=0.0774, fGeo=0.0079, fVol=0.0372),
        'B03':BrdfCoeffs(fIso=0.1306, fGeo=0.0178, fVol=0.0580),
        'B04':BrdfCoeffs(fIso=0.1690, fGeo=0.0227, fVol=0.0574),
        'B08':BrdfCoeffs(fIso=0.3093, fGeo=0.0330, fVol=0.1535),
        'B11':BrdfCoeffs(fIso=0.3430, fGeo=0.0453, fVol=0.1154),
        'B12':BrdfCoeffs(fIso=0.2658, fGeo=0.0387, fVol=0.0639),

    }
}

# Map the bandId attributes given in the XML Viewing_Incidence_Angle_Grids tags
# into a sensible band identification string. I am guessing at the order, 
# as it is undocumented
bandIdMap = {"0":"B01", "1":"B02", "2":"B03", "3":"B04", "4":"B05",
    "5":"B06", "6":"B07", "7":"B08", "8":"B8A", "9":"B09", "10":"B10",
    "11":"B11", "12":"B12"}



def calcRTLSR(kVol, kGeo, bandName, whichCoeffs=BRDFCOEFFS_DEFAULT):
    """
    Calculate the RossThisLiSparse (Reciprocal) BRDF model,
    given the pre-calculated kernels. Parameter set is
    selected by whichCoeffs, and for the given band.
    
    Allowed values for whichCoeffs are sen2l2a_brdf.BRDFCOEFFS_FLOOD2020
    and sen2l2a_brdf.BRDFCOEFFS_ROY2017. 
    
    """
    if whichCoeffs not in brdfCoeffsDict:
        raise Sen2BrdfError("Unknown coefficients '{}' requested".format(whichCoeffs))
    paramSet = brdfCoeffsDict[whichCoeffs]

    if bandName not in paramSet:
        raise Sen2BrdfError("Given band name '{}' not available for '{}' parameters".format(bandName, whichCoeffs))
    p = paramSet[bandName]

    b = p.fIso + p.fVol * kVol + p.fGeo * kGeo
    return b


def calcKgeo(sunZen, satZen, relAz):
    """
    Calculate the BRDF 'geo' kernel, Li-Sparse. Zenith angles are assumed to be 
    non-negative. All angles are in radians. 

    The equations were adopted from MODIS BRDF/Albedo Product: Algorithm 
    Theoretical Basis Document v 5.0

    Fixed parameters br and hb are the b/r and h/b ratios specified 
    in the ATBD (page 14). They represent the crown shape and crown height
    respectively. The values given for these are those specified in 
    the ATBD for the MODIS global processing. 
    """
    br = 1
    hb = 2
    sunZenAdj = numpy.arctan(br*numpy.tan(sunZen))
    satZenAdj = numpy.arctan(br*numpy.tan(satZen))
    phaseAngle = numpy.arccos((numpy.cos(sunZenAdj)*numpy.cos(satZenAdj)) + 
        (numpy.sin(sunZenAdj)*numpy.sin(satZenAdj)*numpy.cos(relAz)))
    D = numpy.sqrt((numpy.tan(sunZenAdj))**2 + (numpy.tan(satZenAdj))**2 -
        (2*numpy.tan(sunZenAdj)*numpy.tan(satZenAdj)*numpy.cos(relAz)))
    cos_t = hb*((numpy.sqrt(D**2 + (numpy.tan(sunZenAdj)*numpy.tan(satZenAdj)*numpy.sin(relAz))**2)) / 
        ((1/numpy.cos(sunZenAdj))+(1/numpy.cos(satZenAdj))))
    cos_t = numpy.clip(cos_t, -1, 1)
    t = numpy.arccos(cos_t)
    O = 1/numpy.pi * ((t-(numpy.sin(t)*numpy.cos(t))) * ((1/numpy.cos(sunZenAdj))+(1/numpy.cos(satZenAdj))))
    Kgeo = (O - (1/numpy.cos(sunZenAdj)) - (1/numpy.cos(satZenAdj)) + 
        (0.5*(1+numpy.cos(phaseAngle))*(1/numpy.cos(sunZenAdj))*(1/numpy.cos(satZenAdj))))
    return Kgeo


def calcKvol(sunZen, satZen, relAz):
    """
    Calculate the BRDF 'vol' kernel, RossThick. Zenith angles are assumed to be 
    non-negative. All angles are in radians. 

    The equations were adopted from MODIS BRDF/Albedo Product: Algorithm 
    Theoretical Basis Document v 5.0
    """
    phaseAngle=numpy.arccos((numpy.cos(sunZen)*numpy.cos(satZen)) + 
        (numpy.sin(sunZen)*numpy.sin(satZen)*numpy.cos(relAz)))
    Kvol=(((((numpy.pi/2) - phaseAngle) * numpy.cos(phaseAngle)) + 
        numpy.sin(phaseAngle)) / (numpy.cos(sunZen) + numpy.cos(satZen))) - (numpy.pi/4)
    return Kvol


class AngleInfo(object):
    """
    Load the angle information from the XML metadata supplied by
    ESA in their L2A package. 

    The view angle grids all fall slightly short at the edges, with 
    null values in 5km grid cells whose centroid is outside the
    imagery swathe. This is problematic for processing right to the
    edge, so the view angle data is dilated (approximately), so that 
    for every image pixel, the corresponding view angle values are 
    non-null

    """
    def __init__(self, zipfilename=None, xmlfilename=None, fileobj=None,
            dilateAngles=True):
        """
        Can be loaded from either the zipfilename, or the explicit 
        XML filename, or from an open file-like object (e.g. a file
        explicitly opened within the zip file). 

        dilateAngles defaults to True, but if False, then omit the
        dilation step, potentially leaving nulls within the swathe. 
        Mainly for testing. 

        """
        # Open the XML file, depending on what we have been given
        if fileobj is not None:
            f = fileobj
        elif xmlfilename is not None:
            f = open(xmlfilename)
        elif zipfile is not None:
            zf = zipfile.ZipFile(zipfilename, 'r')
            filenames = [zi.filename for zi in zf.infolist()]
            safeDirName = [fn for fn in filenames if fn.endswith('.SAFE/')][0]
            granuleSubdir = [fn for fn in filenames if os.path.dirname(fn[:-1]) == (safeDirName+"GRANULE")][0]
            fullmetafilename = [fn for fn in filenames 
                if os.path.dirname(fn) == granuleSubdir[:-1] and 
                fn.endswith("MTD_TL.xml")][0]
            f = zf.open(fullmetafilename)

        # Read and parse the XML
        xmlStr = f.read()
        doc = minidom.parseString(xmlStr)

        geomInfoNode = doc.getElementsByTagName('n1:Geometric_Info')[0]
        # Sun angles. 
        tileAnglesNode = findElementByXPath(geomInfoNode, 'Tile_Angles')[0]
        self.angleGridXres = float(findElementByXPath(tileAnglesNode, 'Sun_Angles_Grid/Zenith/COL_STEP')[0].firstChild.data)
        self.angleGridYres = float(findElementByXPath(tileAnglesNode, 'Sun_Angles_Grid/Zenith/ROW_STEP')[0].firstChild.data)
        self.sunZenithGrid = makeValueArray(findElementByXPath(tileAnglesNode, 'Sun_Angles_Grid/Zenith/Values_List')[0])
        self.sunAzimuthGrid = makeValueArray(findElementByXPath(tileAnglesNode, 'Sun_Angles_Grid/Azimuth/Values_List')[0])

        # Satellite angles
        # Now build up the viewing angle per grid cell, from the 
        # separate layers given for each detector for each band. 
        viewingAngleNodeList = findElementByXPath(tileAnglesNode, 'Viewing_Incidence_Angles_Grids')
        self.viewZenithDict = buildViewAngleArr(viewingAngleNodeList, 'Zenith', dilateAngles)
        self.viewAzimuthDict = buildViewAngleArr(viewingAngleNodeList, 'Azimuth', dilateAngles)

        # Upper-left corners of images at different resolutions. As 
        # far as I can work out, these coords appear to be the upper 
        # left corner of the upper left pixel, i.e. equivalent to GDAL's 
        # convention. This also means that they are the same for the different 
        # resolutions, which is nice. 
        self.ulxyByRes = {}
        geocodingNode = findElementByXPath(geomInfoNode, 'Tile_Geocoding')[0]
        posNodeList = findElementByXPath(geocodingNode, 'Geoposition')
        for posNode in posNodeList:
            res = posNode.getAttribute('resolution')
            ulx = float(findElementByXPath(posNode, 'ULX')[0].firstChild.data.strip())
            uly = float(findElementByXPath(posNode, 'ULY')[0].firstChild.data.strip())
            self.ulxyByRes[res] = (ulx, uly)

        # Dimensions of images at different resolutions. 
        self.dimsByRes = {}
        sizeNodeList = findElementByXPath(geocodingNode, 'Size')
        for sizeNode in sizeNodeList:
            res = sizeNode.getAttribute('resolution')
            nrows = int(findElementByXPath(sizeNode, 'NROWS')[0].firstChild.data.strip())
            ncols = int(findElementByXPath(sizeNode, 'NCOLS')[0].firstChild.data.strip())
            self.dimsByRes[res] = (nrows, ncols)

        # Make a guess at the coordinates of the angle grids. These are not given 
        # explicitly in the XML, but my best guess is that the 5km grid squares
        # extend beyond the imagery by 1/2 a grid square in all directions. So, 
        # the top-left corner of the top-left image pixel is at the centre 
        # of the top-left angle grid square. So, what is calculated 
        # here is the top-left corner of the top-left 5km square. 
        (ulx, uly) = self.ulxyByRes["10"]
        self.anglesULXY = (ulx - self.angleGridXres / 2.0, uly + self.angleGridYres / 2.0)

    def calcResampleNdx(self, pixSize, topY=None, leftX=None, 
            nrows=None, ncols=None):
        """
        Calculate a set of resample index arrays to resample from the 
        5km angle grid to the requested grid. 

        Assumes that the output grid is in the same UTM projection
        as the angle grid and the original imagery. This means we 
        are only changing the pixel size and the grid alignment. 

        The requested grid is specified in terms of the following 
        variables. 

        pixSize = desired output pixel size, in metres (e.g. 10, or 20)
        topY = Y coordinate of the top edge of the topmost pixel
        leftX = X coordinate of the left edge of the leftmost pixel
        nrows = number of rows in output grid
        ncols = number of columns in output grid

        If any of topY, leftX, nrows, or ncols is not given, 
        it defaults to the value for the full-size image for the 
        given pixel size. 

        Returns a tuple of two arrays
            (rowNdx, colNdx)
        This tuple can be used directly as an index into the
        angle arrays to produce resampled arrays on the desired grid.

        """
        resStr = str(pixSize)
        if leftX is None:
            leftX = self.ulxyByRes[resStr][0]
        if topY is None:
            topY = self.ulxyByRes[resStr][1]
        if nrows is None:
            nrows = self.dimsByRes[resStr][0]
        if ncols is None:
            ncols = self.dimsByRes[resStr][1]

        topCtr = topY - pixSize / 2.0
        bottomPlus1Ctr = topCtr - nrows * pixSize
        leftCtr = leftX + pixSize / 2.0
        rightPlus1Ctr = leftCtr + ncols * pixSize
        # Grids of the (X, Y) coords of the output pixel centres
        (yCtr, xCtr) = numpy.mgrid[topCtr:bottomPlus1Ctr:-pixSize,
                                   leftCtr:rightPlus1Ctr:pixSize]
        
        # Now express these relative to the centre of the 
        # top left 5km grid square
        base5kmX = self.anglesULXY[0] + self.angleGridXres / 2.0
        base5kmY = self.anglesULXY[1] - self.angleGridYres / 2.0
        xCtrRel = xCtr - base5kmX
        yCtrRel = base5kmY - yCtr

        # Convert these into index values in the 5km grid
        colNdx = numpy.round(xCtrRel / self.angleGridXres).astype(numpy.uint32)
        rowNdx = numpy.round(yCtrRel / self.angleGridYres).astype(numpy.uint32)

        return (rowNdx, colNdx)

    def calcKernels(self, bandName):
        """
        Calculate the kernels for the RossThickLiSparse(Reciprocal) 
        BRDF model, for the ESA-supplied sun and satellite angles. 
        All calculations are carried out on the complete 5km angle 
        grids, and the returned kernel is an array of the same grid. 

        """
        sunZen = numpy.radians(self.sunZenithGrid)
        sunAz = numpy.radians(self.sunAzimuthGrid)
        if bandName in self.viewZenithDict:
            satZen = self.viewZenithDict[bandName]
            satAz = self.viewAzimuthDict[bandName]
        else:
            raise Sen2BrdfError("Band '{}' not recognised".format(bandName))

        # Convert degrees to radians
        satZen = numpy.radians(satZen)
        satAz = numpy.radians(satAz)

        relAz = (satAz - sunAz)

        kVol = calcKvol(sunZen, satZen, relAz)
        kGeo = calcKgeo(sunZen, satZen, relAz)
        return (kVol, kGeo)
    
    def calcStdAdjustFactor(self, sunZenStdDeg, bandName,
            whichCoeffs=BRDFCOEFFS_DEFAULT):
        """
        Calculate the BRDF adjustment factor, to adjust the given
        band to the given standard sun zenith. It is assumed that
        the standard view zenith is zero, and hence the standard
        relative azimuth is irrelevant. 
        
        All calculation is carried out at the angle grid 
        resolution (5km). 
        
        The sunZenStdDeg is given in degrees. 
        The band name is a string (e.g. 'B02')
        
        The whichCoeffs parameter is used to select which
        set of BRDF coefficients is used. Possible choices
        are sen2l2a_brdf.BRDFCOEFFS_FLOOD2020 or
        sen2l2a_brdf.BRDFCOEFFS_ROY2017. 
        
        """
        # Kernels for standard angles. Scalar. 
        (sunZenStd, satZenStd, relAzStd) = (numpy.radians(sunZenStdDeg), 0.0, 0.0)
        kVolStd = calcKvol(sunZenStd, satZenStd, relAzStd)
        kGeoStd = calcKgeo(sunZenStd, satZenStd, relAzStd)

        (kVol, kGeo) = self.calcKernels(bandName=bandName)

        rStd = calcRTLSR(kVolStd, kGeoStd, bandName, whichCoeffs)
        rImg = calcRTLSR(kVol, kGeo, bandName, whichCoeffs)
        gamma = rStd / rImg
        
        return gamma

    
def buildViewAngleArr(viewingAngleNodeList, angleName, dilateAngles):
    """
    Build up the named viewing angle array from the various 
    detector strips given as separate arrays. 
        
    The angleName is one of 'Zenith' or 'Azimuth'.
    Returns a dictionary of 2-d arrays, keyed by the bandId string, as mapped
    using bandIdMap. 

    Dilates the final array to reduce the nulls at the edges, as described 
    in the class docstring. 

    """
    angleArrDict = {}
    for viewingAngleNode in viewingAngleNodeList:
        bandId = viewingAngleNode.getAttribute('bandId')
        newBandId = bandIdMap[bandId]
        angleNode = findElementByXPath(viewingAngleNode, angleName)[0]
        angleArr = makeValueArray(findElementByXPath(angleNode, 'Values_List')[0])
        if newBandId not in angleArrDict:
            angleArrDict[newBandId] = angleArr
        else:
            mask = (~numpy.isnan(angleArr))
            angleArrDict[newBandId][mask] = angleArr[mask]

    if dilateAngles:
        # Now dilate all bands by expanding at left and right edge of 
        # non-null area. This is not a true dilation, just a horizontal 
        # expansion, but more than sufficient. 
        # Work out the locations to expand using just the first band, 
        # then apply it to all bands (i.e. assume they are all null in 
        # the same places)
        bandNames = sorted(angleArrDict.keys())
        for band in bandNames:
            a = angleArrDict[band]
            nullAtLeftEdge = numpy.where(numpy.isnan(a[:,:-1])&(~numpy.isnan(a[:,1:])))
            ndxOfNonnullLeft = (nullAtLeftEdge[0], nullAtLeftEdge[1] + 1)

            ndxOfNonnullRight = numpy.where(~numpy.isnan(a[:,:-1])&(numpy.isnan(a[:,1:])))
            nullAtRightEdge = (ndxOfNonnullRight[0], ndxOfNonnullRight[1] + 1)

            angleArrDict[band][nullAtLeftEdge] = angleArrDict[band][ndxOfNonnullLeft]
            angleArrDict[band][nullAtRightEdge] = angleArrDict[band][ndxOfNonnullRight]

    return angleArrDict


def findElementByXPath(node, xpath):
    """
    Find a sub-node under the given XML minidom node, by traversing the 
    given XPATH notation.  Searches all possible values, and returns a 
    list of whatever it finds which matches. 
    
    It would be better if minidom understood XPATH, but it does not seem to. 
    I could use some of the other libraries, but ElementTree seems to have 
    obscure bugs, and I did not want to introduce any other dependencies. 
    Sigh.....
    
    """
    nodeNameList = xpath.split('/')
    if len(nodeNameList) > 1:
        nextNodeList = node.getElementsByTagName(nodeNameList[0])
        nodeList = []
        for n in nextNodeList:
            nodeList.extend(findElementByXPath(n, '/'.join(nodeNameList[1:])))
    else:
        nodeList = node.getElementsByTagName(nodeNameList[0])

    return nodeList


def makeValueArray(valuesListNode):
    """
    Take a <Values_List> node from the XML, and return an array of the 
    values contained within it. This will be a 2-d numpy array of 
    float32 values (should I pass the dtype in??)

    """
    valuesList = findElementByXPath(valuesListNode, 'VALUES')
    vals = []
    for valNode in valuesList:
        text = valNode.firstChild.data.strip()
        vals.append([numpy.float32(x) for x in text.split()])
    return numpy.array(vals)


class Sen2BrdfError(Exception): pass
