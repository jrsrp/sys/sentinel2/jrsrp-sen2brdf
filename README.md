# jrsrp-sen2brdf

The Joint Remote Sensing Research Program's modules for applying BRDF corrections to the European Space Agency's Sentinel-2 satellite imagery.

This implements the BRDF standardization procedures outlined in

[Flood, N. (2020). Assessing BRDF effects in the Sentinel-2 Level 2A product in the Australian landscape. Joint Remote Sensing Research Program. ](https://figshare.com/articles/online_resource/Assessing_BRDF_effects_in_the_Sentinel-2_Level_2A_product_in_the_Australian_landscape/12820268)

Users are encouraged to read that document, and to be clear about the limits of the BRDF standardization process. 

## License
Licensed under MIT license. See LICENSE.txt file.

## Requirements
numpy

Test script uses GDAL, but this is not required for operational use. 

## Installation
Installation is managed via the given `pyproject.toml` file. The best way to install is to
use `pip`, with a command such as the following:

```
cd jrsrp-sen2brdf      # i.e. the top directory, where the pyproject.toml is
pip install .
```
Note that  the pip command takes a `--prefix` option, to allow installation in a non-standard
location.

## Testing
A simple test can be performed using the command `testbrdf`. This operates on a pair on nearly coincident images for the same tile, where the two images are from different relative orbits (i.e. adjacent swathes). It reads the zip files and applies the BRDF adjustment, and reports the difference in reflectance between the two swathes, for with and without BRDF adjustment.

## Usage

### Simplest Usage
Simple case, a single band, whole image at once. Standardises to a nadir view, with sun zenith 45 degrees. This is intended to be the simplest to use. 
```
  from jrsrp_sen2brdf import sen2l2a_brdf

  # Read the angle info directly from the L2A zip file
  zipfilename = 'S2A_MSIL2A_20200611T001121_N0214_R073_T55JFL_20200611T022818.zip'
  angleInfo = sen2l2a_brdf.AngleInfo(zipfilename=zipfilename)

  sunZenStd = 45.0
  bandName = 'B02'

  # Row/col index arrays to resample from 5km grid to 10m resolution full-size image
  pixSize = 10
  resample10m = angleInfo.calcResampleNdx(pixSize)

  # BRDF adjustment factor, on full-size 5km angle grid, for this band
  gamma = angleInfo.calcStdAdjustFactor(sunZenStd, bandName)

  # Assume img is numpy array of B02 image data (nrows, ncols), of the whole MGRS tile
  gammaImg = gamma[resample10m]
  img = img * gammaImg
```
Clearly the last three statements can be iterated over the desired bands. The statements above that are independent of the band, and only need to be performed once. If the 20m bands are required, then one would also need a 20m version of the resample index. 

### Processing sub-tiles of full image
This example illustrates the situation where the `img` array is not the whole ESA MGRS image. This might arise when processing in a tiled context such as a tile server, or other tile-based raster processing system. The only change required is that the pixel grid of the desired sub-tile
must be described to the `calcResampleNdx()` function. With this extra information, it also becomes
possible to use a pixelSize other than the standard sizes (10 or 20). This can be useful in 
tile-server contexts. 
```
  resample10mTile = angleInfo.calcResampleNdx(pixSize, topY, leftX, nrows, ncols)
```
Any of the extra parameters not supplied will default to the whole-image value for the pixel size in question. Obviously this is only possible if the pixel size is either 10 or 20. 

**IMPORTANT**: The topY and leftX parameters are the X and Y coordinates of the top-left corner of the top-left pixel, i.e. they are the outer edge of the pixel, NOT its centre. 

### Further Details and Controls
  - The default set of BRDF coefficients is that fitted for the Australian landscape, by Flood (2020). There is an option on the `calcRTLSR()` function to select the coefficients fitted using MODIS imagery by Roy et al (2017). However, these coefficients are not available for the red edge bands. As shown by Flood (2020), the accuracy is comparable, and they may be more appropriate for use outside of Australia. The comparison in Flood (2020) suggests that the process is not particularly sensitive to the precise parameter values. 
  - The `AngleInfo` constructor can read the metadata from either a zip file, as supplied by ESA; or the extracted metadata file (XML, as supplied by ESA) such as the metadata.xml file available in the AWS Sentinel-2 archive; or a file-like object (e.g. an already opened file still within the zipfile).
  - By default, the `AngleInfo` class will grow the edges of the satellite angle grids (on the 5km grid), as the grids supplied by ESA have null values for satellite angles where a pixel is more than half off the edge of the swathe. Since there is valid data in the imagery at these locations, we estimate the angles by expanding from the pixel next door. This is always less than half a pixel. If desired, this behaviour can be turned off using the dilateAngles=False argument in the constructor, but this will result in inaccuracies in those edge pixels, and is intended only for testing and comparative purposes.

### Bibliography
  - Flood, N. (2020). Assessing BRDF effects in the Sentinel-2 Level 2A product in the Australian landscape. Joint Remote Sensing Research Program. 
  - Roy, D., et al. (2017). Examination of Sentinel-2A multi-spectral instrument (MSI) reflectance anisotropy and the suitability of a general method to normalize MSI reflectance to nadir BRDF adjusted reflectance. Remote Sensing of Environment 199. pp 25-38. 
